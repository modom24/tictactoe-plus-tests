function Game() {
}

Game.prototype.count = 0;
Game.prototype.xWin = 0;
Game.prototype.oWin = 0;
Game.prototype.xScore = 0;
Game.prototype.oScore = 0;

var ticTacToe = new Game();

var checkForWin = function (score) {

	var threeInARowConditions = [7, 56, 448, 73, 146, 292, 273, 84];

	for (var i = 0; i < 8; i++) {
		if ((threeInARowConditions[i] & score) == threeInARowConditions[i]) {
			return true;
		}
	}
	return false;
}

var checkForMaxCount = function () {
	if (ticTacToe.count == 9) {
		alert('Its a tie. It will restart.');
		resetGame();
	}
}

var checkForOWin = function () {
		if (checkForWin(ticTacToe.oScore)) {
			alert('O Wins');
			resetGame();
			ticTacToe.oWin += 1;
			$('#o_win').text(ticTacToe.oWin);
		}
}

var checkForXWin = function () {
		if (checkForWin(ticTacToe.xScore)) {
			alert('X Wins');
			resetGame();
			ticTacToe.xWin += 1;
			$('#x_win').text(ticTacToe.xWin);
			return true;
		}
}

var incrementOScore = function(number) {
		ticTacToe.count++;
		ticTacToe.oScore += number;
}

var incrementXScore = function(number) {
		ticTacToe.count++;
		ticTacToe.xScore += number;
}

var checkForDisabledCell = function (cell) {
	if (cell.hasClass('disable')) {
		alert('Already selected');
		return true;
	}
}
	
var checkForTurn = function(Game) {
	if (Game.count % 2 == 0) {
		return true;
	}
	else {
		return false;
	}
}

var resetGame = function () {
	$("#game li").text("");
	$("#game li").removeClass('o');
	$("#game li").removeClass('x');
	$("#game li").removeClass('disable');
	$("#game li").removeClass('btn-primary');
	$("#game li").removeClass('btn-info');
	ticTacToe.xScore = 0;
	ticTacToe.oScore = 0;
	ticTacToe.count = 0;
}

$(document).ready(function () {

    $("#reset").click(function () {
        resetGame();
    });

    $('#game li').click(function () {
        if (checkForTurn(ticTacToe) && !checkForDisabledCell($(this))) {
			incrementOScore(parseInt($(this).attr('id')));
			$(this).text("o");
			$(this).addClass('disable o btn-primary');
			checkForOWin();
        }
        else if (!checkForTurn(ticTacToe) && !checkForDisabledCell($(this))){
			incrementXScore(parseInt($(this).attr('id')));
			$(this).text("x");
			$(this).addClass('disable x btn-info');
			checkForXWin();
        }
        checkForMaxCount();
    });
});