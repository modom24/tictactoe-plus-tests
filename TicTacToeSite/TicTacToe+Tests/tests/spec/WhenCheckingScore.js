describe("When checking that a cell selection increments score correctly", function() {
	beforeEach(function() {
    var ticTacToe = new Game();
	});

  it("x score should have a score of zero.", function() {

	var result = ticTacToe.xScore;
	expect(result).toBe(0);
  });
  
   it("x score should have a score of 5.", function() {
	incrementXScore(5);
	var result = ticTacToe.xScore;
	expect(result).toBe(5);
  });
  
  it("x score should have a score of 20.", function() {
	incrementXScore(10);
	incrementXScore(5);
	var result = ticTacToe.xScore;
	expect(result).toBe(20);
  });
  
  it("o score should have a score of 5.", function() {
	incrementOScore(5)
	var result = ticTacToe.oScore;
	expect(result).toBe(5);
  });
  
    it("o score should have a score of 10.", function() {
	incrementOScore(5)
	var result = ticTacToe.oScore;
	expect(result).toBe(10);
  });
  
    it("o score should have a score of 20.", function() {
	incrementOScore(10)
	var result = ticTacToe.oScore;
	expect(result).toBe(20);
  });
});