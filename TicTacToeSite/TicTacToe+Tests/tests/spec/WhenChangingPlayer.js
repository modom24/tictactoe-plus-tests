describe("When checking that a turn count of 2 selects O as player", function() {
  it("should return true for even turns, false for odd turns.", function() {
		
	var ticTacToe = new Game();
	ticTacToe.count = 2;
			
	var result = checkForTurn(ticTacToe);
	expect(result).toBe(true);
  });
});

describe("When checking that a turn count of 1 selects X as player", function() {
  it("should return true for even turns, false for odd turns.", function() {
		
	var ticTacToe = new Game();
	ticTacToe.count = 1;
			
	var result = checkForTurn(ticTacToe);
	expect(result).toBe(false);
  });
});

describe("When checking that a turn count of 4 selects O as player", function() {
  it("should return true for even turns, false for odd turns.", function() {
		
	var ticTacToe = new Game();
	ticTacToe.count = 4;
			
	var result = checkForTurn(ticTacToe);
	expect(result).toBe(true);
  });
});

describe("When checking that a turn count of 7 selects X as player", function() {
  it("should return true for even turns, false for odd turns.", function() {
		
	var ticTacToe = new Game();
	ticTacToe.count = 7;
			
	var result = checkForTurn(ticTacToe);
	expect(result).toBe(false);
  });
});