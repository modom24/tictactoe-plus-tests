describe("When checking that a score of 7 wins", function() {
  it("returns true if a win has occurred, false otherwise", function() {
		
	var score = 7;
			
	var result = checkForWin(score);
	expect(result).toBe(true);
  });
});

describe("When checking that a score of 84 wins", function() {
  it("returns true if a win has occurred, false otherwise.", function() {
		
	var score = 84;
			
	var result = checkForWin(score);
	expect(result).toBe(true);
  });
});

describe("When checking that a score of 146 wins", function() {
  it("returns true if a win has occurred, false otherwise.", function() {
		
	var score = 146;
			
	var result = checkForWin(score);
	expect(result).toBe(true);
  });
});

describe("When checking that a score of 448 wins", function() {
  it("returns true if a win has occurred, false otherwise.", function() {
		
	var score = 448;
			
	var result = checkForWin(score);
	expect(result).toBe(true);
  });
});

describe("When checking that a score of 5 doesn't win", function() {
  it("returns true if a win has occurred, false otherwise.", function() {
		
	var score = 5;
			
	var result = checkForWin(score);
	expect(result).toBe(false);
  });
});

describe("When checking that a score of 499 doesn't win", function() {
  it("returns true if a win has occurred, false otherwise.", function() {
		
	var score = 499;
			
	var result = checkForWin(score);
	expect(result).toBe(true);
  });
});